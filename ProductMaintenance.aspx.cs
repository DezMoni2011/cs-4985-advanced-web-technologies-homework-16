﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;

/// <summary>
/// Code-behind that handles the events for the ProductMaintenace page.
/// </summary>
/// <author>
/// Destiny Harris
/// </author>
/// <version>
/// March 24, 2015 | Spring
/// </version>
public partial class ProductMaintenance : Page
{
    /// <summary>
    /// Handles the Load event of the Page control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="EventArgs" /> instance containing the event data.</param>
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    /// <summary>
    /// Handles the ItemUpdated event of the lvProductMaintainer control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="ListViewUpdatedEventArgs"/> instance containing the event data.</param>
    protected void lvProductMaintainer_ItemUpdated(object sender, ListViewUpdatedEventArgs e)
    {
        if (e.Exception != null)
        {
            this.lblError.Text = "A database issue occured.<br /><br />" + "Messeage : " + e.Exception.Message;
            e.KeepInEditMode = true;
            e.ExceptionHandled = true;
        }
        else if (e.AffectedRows == 0)
        {
            this.lblError.Text = "Another user may have updated that category." +
                "<br />Please try again.";
        }
    }
    /// <summary>
    /// Handles the ItemDeleted event of the lvProductMaintainer control.
    /// </summary>
    /// <param name="sender">The source of the event.</param>
    /// <param name="e">The <see cref="ListViewDeletedEventArgs"/> instance containing the event data.</param>
    protected void lvProductMaintainer_ItemDeleted(object sender, ListViewDeletedEventArgs e)
    {
         if (e.Exception != null)
        {
            this.lblError.Text = "A database error has occurred.<br /><br />" +
                "Message: " + e.Exception.Message;
            e.ExceptionHandled = true;
        }
        else if (e.AffectedRows == 0)
            this.lblError.Text = "Another user may have updated that category." +
                "<br />Please try again.";
    }
}
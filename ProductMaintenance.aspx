﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ProductMaintenance.aspx.cs" Inherits="ProductMaintenance" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
    <head runat="server">
        <title>Chapter 16 : Northwind</title>
        <link href="Styles/Main.css" rel="stylesheet" />
    </head>
    <body>
        <header></header>

        <section>
           <form id="mainForm" runat="server">
               <asp:ListView ID="lvProductMaintainer" runat="server" 
                   DataKeyNames="ProductID" DataSourceID="sdsNorthwindProducts" 
                   OnItemUpdated="lvProductMaintainer_ItemUpdated" 
                   OnItemDeleted="lvProductMaintainer_ItemDeleted">
                   <AlternatingItemTemplate>
                       <table id="alternatingItemTemplate">
                           <tr>
                               <td>   
                                    ProductID:
                               </td>
                               <td>
                                    <asp:Label ID="lblProductID" runat="server" Text='<%# Eval("ProductID") %>' />                                   
                               </td>
                           </tr>
                           <tr>
                               <td>
                                    ProductName:
                               </td>
                               <td>
                                    <asp:Label ID="lblProductName" runat="server" Text='<%# Eval("ProductName") %>' />
                               </td>
                           </tr>
                           <tr>
                               <td>
                                    SupplierID:
                               </td>
                               <td>
                                    <asp:Label ID="lblSupplierID" runat="server" Text='<%# Eval("SupplierID") %>' /> 
                               </td>
                           </tr>
                           <tr>
                               <td>
                                    CategoryID:
                               </td>
                               <td>
                                    <asp:Label ID="lblCategoryID" runat="server" Text='<%# Eval("CategoryID") %>' />
                               </td>
                           </tr>
                           <tr>
                               <td>
                                    QuantityPerUnit:
                               </td>
                               <td>
                                    <asp:Label ID="lblQuantityPerUnit" runat="server" Text='<%# Eval("QuantityPerUnit") %>' /> 
                               </td>
                           </tr>
                           <tr>
                               <td>
                                    UnitPrice:
                               </td>
                               <td>
                                    <asp:Label ID="lblUnitPrice" runat="server" Text='<%# Eval("UnitPrice", "{0:C}") %>' />
                               </td>
                           </tr>
                           <tr>
                               <td>
                                    UnitsInStock:
                               </td>
                               <td>
                                    <asp:Label ID="lblUnitsInStock" runat="server" Text='<%# Eval("UnitsInStock") %>' /> 
                               </td>
                           </tr>
                           <tr>
                               <td>
                                    UnitsOnOrder:
                               </td>
                               <td>
                                   <asp:Label ID="lblUnitsOnOrder" runat="server" Text='<%# Eval("UnitsOnOrder") %>' />
                               </td>
                           </tr>
                           <tr>
                               <td>
                                    ReorderLevel:
                               </td>
                               <td>
                                    <asp:Label ID="lblReorderLevel" runat="server" Text='<%# Eval("ReorderLevel") %>' /> 
                               </td>
                           </tr>
                           <tr>
                               <td>
                                   <asp:CheckBox ID="cbxDiscontinued" runat="server" 
                                       Checked='<%# Eval("Discontinued") %>' 
                                       Enabled="false" 
                                       Text="Discontinued" />
                               </td>
                           </tr>
                       </table>
                       <asp:Button ID="btnEdit" runat="server" CommandName="Edit" Text="Edit" />
                       <asp:Button ID="btnDelete" runat="server" CommandName="Delete" Text="Delete" />
                       <br /><br />
                   </AlternatingItemTemplate>
                   <EditItemTemplate>
                        <table id="editItemTemplate">
                           <tr>
                               <td>   
                                    ProductID:
                               </td>
                               <td>
                                    <asp:Label ID="lblProductID" runat="server" 
                                        Text='<%# Eval("ProductID") %>' />                                  
                               </td>
                           </tr>
                           <tr>
                               <td>
                                    ProductName:
                               </td>
                               <td>
                                    <asp:TextBox ID="txtProductName" runat="server" 
                                        Text='<%# Bind("ProductName") %>' />
                                    <asp:RequiredFieldValidator ID="rfvProductName" runat="server" 
                                        ErrorMessage="Please specify a product name." 
                                        Display="Dynamic" CssClass="asterick" 
                                        Text="*" 
                                        ControlToValidate="txtProductName">
                                    </asp:RequiredFieldValidator>
                               </td>
                           </tr>
                           <tr>
                               <td>
                                    SupplierID:
                               </td>
                               <td>
                                     <asp:TextBox ID="txtSupplierID" runat="server" 
                                         Text='<%# Bind("SupplierID") %>' />
                                     <asp:RequiredFieldValidator ID="rfvSupplierID" runat="server" 
                                         ErrorMessage="Must specify the suppplier id." 
                                         Display="Dynamic" 
                                         CssClass="asterick" 
                                         Text="*" 
                                         ControlToValidate="txtSupplierID">
                                     </asp:RequiredFieldValidator>
                                    <asp:CompareValidator ID="cvSupplierID" runat="server" 
                                        ErrorMessage="Suppiler id must be a positive number" 
                                        CssClass="asterick" Display="Dynamic" 
                                        Text="*" 
                                        ControlToValidate="txtSupplierID" 
                                        ValueToCompare="0" 
                                        Operator="GreaterThan" 
                                        Type="Integer">
                                    </asp:CompareValidator>
                              </td>
                           </tr>
                           <tr>
                               <td>
                                    CategoryID:
                               </td>
                               <td>
                                    <asp:TextBox ID="txtCategoryID" runat="server" 
                                        Text='<%# Bind("CategoryID") %>' />
                                    <asp:RequiredFieldValidator ID="rfvCategoryID" runat="server" 
                                        ErrorMessage="Must specify the category id." 
                                        Display="Dynamic" 
                                        Text="*" 
                                        ControlToValidate="txtCategoryID" 
                                        CssClass="asterick">
                                    </asp:RequiredFieldValidator>
                                    <asp:CompareValidator ID="cvCategoryID" runat="server" 
                                        ErrorMessage="Category id must be a positive number" 
                                        CssClass="asterick" 
                                        Display="Dynamic" 
                                        Text="*" 
                                        ControlToValidate="txtCategoryID" 
                                        ValueToCompare="0" 
                                        Operator="GreaterThan" 
                                        Type="Integer">
                                    </asp:CompareValidator>
                               </td>
                           </tr>
                           <tr>
                               <td>
                                    QuantityPerUnit:
                               </td>
                               <td>
                                    <asp:TextBox ID="txtQuantityPerUnit" runat="server" 
                                        Text='<%# Bind("QuantityPerUnit") %>' />
                                    <asp:RequiredFieldValidator ID="rfvQuantityPerUnit" runat="server" 
                                        ErrorMessage="Please specify the quantity per unit." 
                                        Display="Dynamic" 
                                        Text="*" 
                                        ControlToValidate="txtQuantityPerUnit" 
                                        CssClass="asterick">
                                    </asp:RequiredFieldValidator>
                                    </td>
                           </tr>
                           <tr>
                               <td>
                                    UnitPrice:
                               </td>
                               <td>
                                    <asp:TextBox ID="txtUnitPrice" runat="server" 
                                        Text='<%# Bind("UnitPrice") %>' />
                                    <asp:RequiredFieldValidator ID="rfvUnitPrice" runat="server" 
                                        ErrorMessage="Must specify a unit price." 
                                        CssClass="asterick" 
                                        Display="Dynamic" 
                                        Text="*" 
                                        ControlToValidate="txtUnitPrice">
                                    </asp:RequiredFieldValidator>
                                   <asp:CompareValidator ID="cvUnitPrice" runat="server" 
                                       ErrorMessage="Unit price must be a currency." 
                                       Display="Dynamic" 
                                       CssClass="asterick" 
                                       Text="*" 
                                       ControlToValidate="txtUnitPrice" 
                                       Operator="DataTypeCheck" 
                                       Type="Currency">
                                   </asp:CompareValidator>
                               </td>
                           </tr>
                           <tr>
                               <td>
                                    UnitsInStock:
                               </td>
                               <td>
                                    <asp:TextBox ID="txtUnitsInStock" runat="server" 
                                        Text='<%# Bind("UnitsInStock") %>' />
                                    <asp:RequiredFieldValidator ID="rfvUnitsInStock" runat="server" 
                                        ErrorMessage="Please specify the number of units in stock." 
                                        CssClass="asterick" 
                                        Display="Dynamic" 
                                        Text="*" 
                                        ControlToValidate="txtUnitsInStock">
                                    </asp:RequiredFieldValidator>
                                   <asp:CompareValidator ID="cvUnitsInStock" runat="server" 
                                       ErrorMessage="Units in stock must be a positive number." 
                                       CssClass="asterick" 
                                       Display="Dynamic" 
                                       Text="*" 
                                       ControlToValidate="txtUnitsInStock" 
                                       Type="Integer" 
                                       ValueToCompare="0" 
                                       Operator="GreaterThanEqual">
                                   </asp:CompareValidator>
                               </td>
                           </tr>
                           <tr>
                               <td>
                                    UnitsOnOrder:
                               </td>
                               <td>
                                   <asp:TextBox ID="txtUnitsOnOrder" runat="server" 
                                       Text='<%# Bind("UnitsOnOrder") %>' />
                                   <asp:RequiredFieldValidator ID="rfvUnitsOnOrder" runat="server" 
                                       ErrorMessage="Please specify the number of units on an order." 
                                       CssClass="asterick" 
                                       Display="Dynamic" 
                                       Text="*" 
                                       ControlToValidate="txtUnitsOnOrder">
                                   </asp:RequiredFieldValidator>
                                   <asp:CompareValidator ID="cvUnitsOnOrder" runat="server" 
                                       ErrorMessage="Units on order must be represented as a postive number." 
                                       CssClass="asterick" 
                                       Display="Dynamic" 
                                       Text="*" 
                                       ControlToValidate="txtUnitsOnOrder" 
                                       Type="Integer" 
                                       Operator="GreaterThanEqual" 
                                       ValueToCompare="0">
                                   </asp:CompareValidator>
                               </td>
                           </tr>
                           <tr>
                               <td>
                                    ReorderLevel:
                               </td>
                               <td>
                                    <asp:TextBox ID="txtReoerderLevel" runat="server" 
                                        Text='<%# Bind("ReorderLevel") %>'/> 
                                    <asp:RequiredFieldValidator ID="rfvReoerderLevel" runat="server" 
                                        ErrorMessage="Please specify the re-order level" 
                                        CssClass="asterick" 
                                        Display="Dynamic" 
                                        Text="*" 
                                        ControlToValidate="txtReoerderLevel">
                                    </asp:RequiredFieldValidator>
                                    <asp:CompareValidator ID="cvReoerderLevel" runat="server" 
                                        ErrorMessage="Re-order level must be represented as a postive number." 
                                        CssClass="asterick" 
                                        Display="Dynamic" 
                                        Text="*" 
                                         ControlToValidate="txtReoerderLevel"  
                                        Type="Integer"
                                        Operator="GreaterThanEqual" 
                                        ValueToCompare="0">
                                    </asp:CompareValidator>
                               </td>
                           </tr>
                           <tr>
                               <td>
                                   <asp:CheckBox ID="cbxDiscontinued" runat="server" 
                                       Checked='<%# Bind("Discontinued") %>' 
                                       Text="Discontinued" />
                               </td>
                           </tr>
                       </table>
                       <asp:Button ID="btnUpdate" runat="server" CommandName="Update" Text="Update" />
                       <asp:Button ID="btnCancel" runat="server" CommandName="Cancel" Text="Cancel" />
                       <br /><br />
                   </EditItemTemplate>
                   <EmptyDataTemplate>
                       <span>No data was returned.</span>
                   </EmptyDataTemplate>
                   <InsertItemTemplate>
                       <table id="insertItemTemplate">
                           <tr>
                               <td>   
                                    ProductID:
                               </td>
                               <td>
                                    <asp:TextBox ID="txtProductID" runat="server" Text='<%# Bind("ProductID") %>' />                                  
                               </td>
                           </tr>
                           <tr>
                               <td>
                                    ProductName:
                               </td>
                               <td>
                                    <asp:TextBox ID="txtProductName" runat="server" Text='<%# Bind("ProductName") %>' />
                               </td>
                           </tr>
                           <tr>
                               <td>
                                    SupplierID:
                               </td>
                               <td>
                                     <asp:TextBox ID="txtSupplierID" runat="server" Text='<%# Bind("SupplierID") %>' />
                               </td>
                           </tr>
                           <tr>
                               <td>
                                    CategoryID:
                               </td>
                               <td>
                                    <asp:TextBox ID="txtCategoryID" runat="server" Text='<%# Bind("CategoryID") %>' />
                               </td>
                           </tr>
                           <tr>
                               <td>
                                    QuantityPerUnit:
                               </td>
                               <td>
                                    <asp:TextBox ID="txtQuantityPerUnit" runat="server" Text='<%# Bind("QuantityPerUnit") %>' />
                               </td>
                           </tr>
                           <tr>
                               <td>
                                    UnitPrice:
                               </td>
                               <td>
                                    <asp:TextBox ID="txtUnitPrice" runat="server" Text='<%# Bind("UnitPrice") %>' />
                               </td>
                           </tr>
                           <tr>
                               <td>
                                    UnitsInStock:
                               </td>
                               <td>
                                    <asp:TextBox ID="txtUnitsInStock" runat="server" Text='<%# Bind("UnitsInStock") %>' />
                               </td>
                           </tr>
                           <tr>
                               <td>
                                    UnitsOnOrder:
                               </td>
                               <td>
                                   <asp:TextBox ID="txtUnitsOnOrder" runat="server" Text='<%# Bind("UnitsOnOrder") %>' />
                               </td>
                           </tr>
                           <tr>
                               <td>
                                    ReorderLevel:
                               </td>
                               <td>
                                    <asp:TextBox ID="txtReoerderLevel" runat="server" Text='<%# Bind("ReorderLevel") %>' /> 
                               </td>
                           </tr>
                           <tr>
                               <td>
                                   <asp:CheckBox ID="cbxDiscontinued" runat="server" Checked='<%# Bind("Discontinued") %>' Text="Discontinued" />
                               </td>
                           </tr>
                       </table>
                       <asp:Button ID="btnInsert" runat="server" CommandName="Insert" Text="Insert" />
                       <asp:Button ID="btnClear" runat="server" CommandName="Cancel" Text="Clear" />
                       <br /><br />
                   </InsertItemTemplate>
                   <ItemTemplate>
                       <table id="itemTemplate">
                           <tr>
                               <td>   
                                    ProductID:
                               </td>
                               <td>
                                    <asp:Label ID="lblProductID" runat="server" Text='<%# Eval("ProductID") %>' />                                   
                               </td>
                           </tr>
                           <tr>
                               <td>
                                    ProductName:
                               </td>
                               <td>
                                    <asp:Label ID="lblProductName" runat="server" Text='<%# Eval("ProductName") %>' />
                               </td>
                           </tr>
                           <tr>
                               <td>
                                    SupplierID:
                               </td>
                               <td>
                                    <asp:Label ID="lblSupplierID" runat="server" Text='<%# Eval("SupplierID") %>' /> 
                               </td>
                           </tr>
                           <tr>
                               <td>
                                    CategoryID:
                               </td>
                               <td>
                                    <asp:Label ID="lblCategoryID" runat="server" Text='<%# Eval("CategoryID") %>' />
                               </td>
                           </tr>
                           <tr>
                               <td>
                                    QuantityPerUnit:
                               </td>
                               <td>
                                    <asp:Label ID="lblQuantityPerUnit" runat="server" Text='<%# Eval("QuantityPerUnit") %>' /> 
                               </td>
                           </tr>
                           <tr>
                               <td>
                                    UnitPrice:
                               </td>
                               <td>
                                    <asp:Label ID="lblUnitPrice" runat="server" Text='<%# Eval("UnitPrice", "{0:C}") %>' />
                               </td>
                           </tr>
                           <tr>
                               <td>
                                    UnitsInStock:
                               </td>
                               <td>
                                    <asp:Label ID="lblUnitsInStock" runat="server" Text='<%# Eval("UnitsInStock") %>' /> 
                               </td>
                           </tr>
                           <tr>
                               <td>
                                    UnitsOnOrder:
                               </td>
                               <td>
                                   <asp:Label ID="lblUnitsOnOrder" runat="server" Text='<%# Eval("UnitsOnOrder") %>' />
                               </td>
                           </tr>
                           <tr>
                               <td>
                                    ReorderLevel:
                               </td>
                               <td>
                                    <asp:Label ID="lblReorderLevel" runat="server" Text='<%# Eval("ReorderLevel") %>' /> 
                               </td>
                           </tr>
                           <tr>
                               <td>
                                   <asp:CheckBox ID="cbxDiscontinued" runat="server" Checked='<%# Eval("Discontinued") %>' Enabled="false" Text="Discontinued" />
                               </td>
                           </tr>
                       </table>
                       <asp:Button ID="btnEdit" runat="server" CommandName="Edit" Text="Edit" />
                       <asp:Button ID="btnDelete" runat="server" CommandName="Delete" Text="Delete" />
                       <br /><br />
                   </ItemTemplate>
                   <LayoutTemplate>
                       <div id="itemPlaceholderContainer" runat="server">
                           <span runat="server" id="itemPlaceholder" />
                       </div>
                       <div id="dataPagerBackground">
                           <asp:DataPager ID="dpProducts" runat="server" PageSize="1">
                               <Fields>
                                   <asp:NextPreviousPagerField ButtonType="Button" ShowFirstPageButton="True" ShowLastPageButton="True" />
                               </Fields>
                           </asp:DataPager>
                       </div>
                   </LayoutTemplate>
                   <SelectedItemTemplate>
                       <table id="selectedItemTemplate">
                           <tr>
                               <td>   
                                    ProductID:
                               </td>
                               <td>
                                    <asp:Label ID="lblProductID" runat="server" Text='<%# Eval("ProductID") %>' />                                   
                               </td>
                           </tr>
                           <tr>
                               <td>
                                    ProductName:
                               </td>
                               <td>
                                    <asp:Label ID="lblProductName" runat="server" Text='<%# Eval("ProductName") %>' />
                               </td>
                           </tr>
                           <tr>
                               <td>
                                    SupplierID:
                               </td>
                               <td>
                                    <asp:Label ID="lblSupplierID" runat="server" Text='<%# Eval("SupplierID") %>' /> 
                               </td>
                           </tr>
                           <tr>
                               <td>
                                    CategoryID:
                               </td>
                               <td>
                                    <asp:Label ID="lblCategoryID" runat="server" Text='<%# Eval("CategoryID") %>' />
                               </td>
                           </tr>
                           <tr>
                               <td>
                                    QuantityPerUnit:
                               </td>
                               <td>
                                    <asp:Label ID="lblQuantityPerUnit" runat="server" Text='<%# Eval("QuantityPerUnit") %>' /> 
                               </td>
                           </tr>
                           <tr>
                               <td>
                                    UnitPrice:
                               </td>
                               <td>
                                    <asp:Label ID="lblUnitPrice" runat="server" Text='<%# Eval("UnitPrice", "{0:C}") %>' />
                               </td>
                           </tr>
                           <tr>
                               <td>
                                    UnitsInStock:
                               </td>
                               <td>
                                    <asp:Label ID="lblUnitsInStock" runat="server" Text='<%# Eval("UnitsInStock") %>' /> 
                               </td>
                           </tr>
                           <tr>
                               <td>
                                    UnitsOnOrder:
                               </td>
                               <td>
                                   <asp:Label ID="lblUnitsOnOrder" runat="server" Text='<%# Eval("UnitsOnOrder") %>' />
                               </td>
                           </tr>
                           <tr>
                               <td>
                                    ReorderLevel:
                               </td>
                               <td>
                                    <asp:Label ID="lblReorderLevel" runat="server" Text='<%# Eval("ReorderLevel") %>' /> 
                               </td>
                           </tr>
                           <tr>
                               <td>
                                   <asp:CheckBox ID="cbxDiscontinued" runat="server" Checked='<%# Eval("Discontinued") %>' Enabled="false" Text="Discontinued" />
                               </td>
                           </tr>
                       </table>
                       <asp:Button ID="btnEdit" runat="server" CommandName="Edit" Text="Edit" />
                       <asp:Button ID="btnDelete" runat="server" CommandName="Delete" Text="Delete" />
                       <br /><br />
                   </SelectedItemTemplate>
               </asp:ListView>
               <asp:SqlDataSource ID="sdsNorthwindProducts" runat="server" 
                   ConnectionString="<%$ ConnectionStrings:NorthwindConnectionString %>" 
                   ProviderName="<%$ ConnectionStrings:NorthwindConnectionString.ProviderName %>"  
                   DeleteCommand="DELETE FROM [tblProducts] 
                        WHERE [ProductID] = ?" 
                   InsertCommand="INSERT INTO [tblProducts] 
                        ([ProductID], [ProductName], [SupplierID], [CategoryID], [QuantityPerUnit], 
                         [UnitPrice], [UnitsInStock], [UnitsOnOrder], [ReorderLevel], [Discontinued]) 
                        VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)"
                   SelectCommand="SELECT * FROM [tblProducts]" 
                   UpdateCommand="UPDATE [tblProducts] 
                        SET [ProductName] = ?, [SupplierID] = ?, [CategoryID] = ?, 
                            [QuantityPerUnit] = ?, [UnitPrice] = ?, [UnitsInStock] = ?, 
                            [UnitsOnOrder] = ?, [ReorderLevel] = ?, [Discontinued] = ? 
                        WHERE [ProductID] = ?">
                   <DeleteParameters>
                       <asp:Parameter Name="ProductID" Type="Int32" />
                   </DeleteParameters>
                   <InsertParameters>
                       <asp:Parameter Name="ProductID" Type="Int32" />
                       <asp:Parameter Name="ProductName" Type="String" />
                       <asp:Parameter Name="SupplierID" Type="Int32" />
                       <asp:Parameter Name="CategoryID" Type="Int32" />
                       <asp:Parameter Name="QuantityPerUnit" Type="String" />
                       <asp:Parameter Name="UnitPrice" Type="Decimal" />
                       <asp:Parameter Name="UnitsInStock" Type="Int16" />
                       <asp:Parameter Name="UnitsOnOrder" Type="Int16" />
                       <asp:Parameter Name="ReorderLevel" Type="Int16" />
                       <asp:Parameter Name="Discontinued" Type="Boolean" />
                   </InsertParameters>
                   <UpdateParameters>
                       <asp:Parameter Name="ProductName" Type="String" />
                       <asp:Parameter Name="SupplierID" Type="Int32" />
                       <asp:Parameter Name="CategoryID" Type="Int32" />
                       <asp:Parameter Name="QuantityPerUnit" Type="String" />
                       <asp:Parameter Name="UnitPrice" Type="Decimal" />
                       <asp:Parameter Name="UnitsInStock" Type="Int16" />
                       <asp:Parameter Name="UnitsOnOrder" Type="Int16" />
                       <asp:Parameter Name="ReorderLevel" Type="Int16" />
                       <asp:Parameter Name="Discontinued" Type="Boolean" />
                       <asp:Parameter Name="ProductID" Type="Int32" />
                   </UpdateParameters>
               </asp:SqlDataSource>

               <asp:ValidationSummary ID="vsProducts" CssClass="error" runat="server" HeaderText="Please correct the following errors." />
               <asp:Label ID="lblError" CssClass="error" runat="server"></asp:Label>
            </form> 
        </section>
    </body>
</html>
